declare namespace reactive {
    export interface Subscription {
        request(n: number): void;
        cancel(): void;
    }

    export interface Subscriber<T> {
        onSubscribe(subscription: Subscription): void;
        onNext(value: T): void;
        onError(error: Error): void;
        onComplete(): void;
    }

    export interface Publisher<T> {
        subscribe(subscriber: Subscriber<T>): void
    }
}

export = reactive;
